/**
 * VMS API Tester javascript module.
 * @class VMS
 * @author Atanas Hristov
 * @docauthor Atanas Hristov
 */

VMS = {};

/**
 * The IP address of the VMS
 *
 * @property {String}
 *
 */
VMS.IP_ADDR = '192.168.1.4';

/**
 * The MAC address of the VMS
 *
 * @property {String}
 *
 */
VMS.MAC_ADDR = 'cc:7d:37:b5:9f:1b';


/**
 * Implements namespacing.
 *
 * Usage example:
 *
 * 		VMS.namespace('VMS.UI.Windows');
 *
 *
 * @param {String} ns_string Namespace in the form of 'Lib.Path.Path'.
 */
VMS.namespace = function (ns_string) {
	var parts = ns_string.split('.'),
		parent = VMS,
		i,
		len;

	// strip redundant leading global
	if (parts[0] === 'VMS') {
		parts = parts.slice(1);
	}

	for (i = 0, len=parts.length; i < len; i += 1) {
		// create a property if it does not exist
		if (typeof parent[parts[i]] === "undefined") {
			parent[parts[i]] = {};
		}
		parent = parent[parts[i]];
	}

	return parent;
};
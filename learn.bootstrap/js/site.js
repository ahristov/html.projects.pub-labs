
VMS.namespace('VMS.Console.Site');


/**
 * A shortcut to post JSON via JQuery.
 *
 * @param {String} url Url to post to
 * @param {Object} data JSON to post
 * @param {Function} callback Run when either error or success, gets two params: textStatus and responseJSON, both are strings.
 */
$.postJSON = function(url, data, callback) {

	console.log("POST to: " + url);

	$.ajax({
		url: url,
		type: 'POST',
		contentType : "application/x-www-form-urlencoded",
		data: data,
		dataType: 'json',
		error: function(XMLHttpRequest, textStatus, errorTrown) { callback(textStatus, {}); },
		success: function(data, textStatus) { callback(textStatus, data); }
	});
};


/**
 *
 * This is a copy of the JSON formatter of Jon Combe.
 *
 * **URL** : http://joncom.be/code/javascript-json-formatter/
 *
 * @param {Object} oData JSON object to format
 * @param {String} sIndent
 * @return {String} formated JSON string
 */
VMS.Console.Site.formatJSON = function(oData, sIndent) {

	sIndent = sIndent || ' ';

	// http://joncom.be/code/realtypeof/
	function RealTypeOf(v) {
		if (typeof(v) == "object") {
			if (v === null) return "null";
			if (v.constructor == (new Array).constructor) return "array";
			if (v.constructor == (new Date).constructor) return "date";
			if (v.constructor == (new RegExp).constructor) return "regex";
			return "object";
		}
		return typeof(v);
	}


	// http://joncom.be/code/javascript-json-formatter/
	function SortObject(oData) {
		var oNewData = {};
		var aSortArray = [];

		// sort keys
		_.each(_.keys(oData), function(sKey) {
			aSortArray.push(sKey);
		});
		aSortArray.sort(SortLowerCase);

		// create new data object
		_.each(aSortArray, function(i) {
			if (RealTypeOf(oData[(aSortArray[i])]) == "object" ) {
				oData[(aSortArray[i])] = SortObject(oData[(aSortArray[i])]);
			}
			oNewData[(aSortArray[i])] = oData[(aSortArray[i])];
		});

		return oNewData;

		function SortLowerCase(a,b) {
			a = a.toLowerCase();
			b = b.toLowerCase();
			return ((a < b) ? -1 : ((a > b) ? 1 : 0));
		}
	}

	function FormatJSON(oData, sIndent) {
		if (arguments.length < 2) {
			var sIndent = "";
		}
		var sIndentStyle = "    ";
		var sDataType = RealTypeOf(oData);

		// open object
		if (sDataType == "array") {
			if (oData.length == 0) {
				return "[]";
			}
			var sHTML = "[";
		} else {
			var iCount = 0;
			_.each(_.keys(oData), function() {
				iCount++;
				return;
			});
			if (iCount == 0) { // object is empty
				return "{}";
			}
			var sHTML = "{";
		}

		// loop through items
		var iCount = 0;
		_.each(_.keys(oData), function(sKey) {
			if (iCount > 0) {
				sHTML += ",";
			}
			if (sDataType == "array") {
				sHTML += ("\n" + sIndent + sIndentStyle);
			} else {
				sHTML += ("\n" + sIndent + sIndentStyle + "\"" + sKey + "\"" + ": ");
			}

			var vValue = oData[sKey];

			// display relevant data type
			switch (RealTypeOf(vValue)) {
				case "array":
				case "object":
					sHTML += FormatJSON(vValue, (sIndent + sIndentStyle));
					break;
				case "boolean":
				case "number":
					sHTML += vValue.toString();
					break;
				case "null":
					sHTML += "null";
					break;
				case "string":
					sHTML += ("\"" + vValue + "\"");
					break;
				default:
					sHTML += ("TYPEOF: " + typeof(vValue));
			}

			// loop
			iCount++;
		});

		// close object
		if (sDataType == "array") {
			sHTML += ("\n" + sIndent + "]");
		} else {
			sHTML += ("\n" + sIndent + "}");
		}

		// return
		return sHTML;
	}

	return FormatJSON(oData, sIndent);
};




/**
 * Fill the select box passed in as parameter with the
 * command options from the API service group given by name
 */
VMS.Console.Site.setUpApiServiceCommandOptions = function() {

	var apiServiceGroupName = $("#api_service_group").val();
	var selBox = $('#api_service_command');

	$(selBox).empty();

	if (apiServiceGroupName === 'MISC') {
		$(selBox).append('<option value="INT_SLEEP">INT_SLEEP</option>');
		return;
	}

	_.each(
		_.sortBy(
			VMS.Test.ConsoleManuals[apiServiceGroupName],
			function(command) { return command.ServiceName }
		),
		function(command) {
			$(selBox).append('<option value="'+command.ServiceName+'">'+command.ServiceName+'</option>');
		}
	);

};


/**
 * Setup Plugins like dropdown, in-place-editor, etc.
 * for all the corresponding DHTML elements.
 */
VMS.Console.Site.setUpDhtmlPlugins = function() {

	var me = this;

	$('.dropdown-toggle').dropdown();

	$('code.request-json').editInPlace({

		field_type: 'textarea',

		callback: function() {

			var resultHtml;
			var sortable = $(this).closest('li')[0];

			try {

				var json = JSON.parse(this.innerHTML);
				resultHtml = JSON.stringify(json).replace(/,"/ig, ', "');
				sortable.CommandData.requestJSON = json;

			}
			catch (e) {

				json = sortable.CommandData.requestJSON;
				resultHtml = JSON.stringify(json).replace(/,"/ig, ', "');

				var alertMsg = $(
					'<div class="alert-error block-message error">' +
					'<a class="close" data-dismiss="alert" href="#">&times;</a>' +
					'<p><strong>Sorry!</strong> Please enter valid JSON.</p>' +
					'</div>'
				);

				$(this).after(alertMsg);
				me.createAutoClosingAlert(alertMsg, 2000);

			}

			return resultHtml;
		}
	});

};

/**
 * Creates autoclosing alert.
 *
 * @param {String} alert jQuery html element.
 * @param {Number} delay miliseconds to close after
 */
VMS.Console.Site.createAutoClosingAlert = function(alert, delay) {

	alert.alert();
	setTimeout(function() { alert.remove(); }, delay);

};


/**
 * Gets the URL of the service accordingly to the IP address
 * and the API service commands group name.
 *
 * @param {String} groupName The name of the API service commands group
 * @return {String} url to the VMS service
 */
VMS.Console.Site.getServiceUrl = function(groupName) {

	var vmsIPAddress = $('#vms_ip_addr').val();
	return "http://" + vmsIPAddress + "/" + groupName + "/";

};


/**
 * Hide/show effect on the used thru the code.
 *
 * @param {HTMLElement} htmlElement Usually a sortable element.
 */
VMS.Console.Site.hideAndShow = function(htmlElement) {

	$(htmlElement).hide("fade", {}, 200);
	$(htmlElement).show("fade", {}, 100);

};


/**
 * Create DOM Sortable Object for one command.
 *
 * NOTE: The returned object has CommandData property with members:
 *
 * - groupName
 * the name of the API commands group
 *
 * - commandName
 * the name of the API command
 *
 * - requestJSON
 * the API command's JSON
 *
 * - responseJSON
 * the response JSON from the service, or {} if it was a new command
 *
 * - responseStatus
 * "New", "Error" or "Success" accordingly to the responseJSON object.
 *
 * @param {String} groupName Name of the API commands group
 * @param {String} commandName Name of the API command
 * @param {Object} requestJSON The JSON to send
 * @param {Object} responseJSON The reseived JSON
 * @return {HTMLElement} DOM object to add to the sortables.
 */
VMS.Console.Site.createSortable = function(groupName, commandName, requestJSON, responseJSON) {

	var templateText = $("#template-command").html();
	var returnStatusPropertyName = VMS.Test.ReturnStatusPropertyName[groupName];

	var responseStatus = "New";
	if (_.has(responseJSON, returnStatusPropertyName)) {
		responseStatus = responseJSON[returnStatusPropertyName]
			? "Error"
			: "Success";
	}

	var commandData = {
		groupName: groupName,
		commandName: commandName,
		requestJSON: requestJSON,
		responseJSON: responseJSON,
		responseStatus: responseStatus
	};

	var templateHtml = _.template(templateText, commandData);

	var sortable = $(templateHtml)[0];
	sortable.CommandData = commandData;

	return sortable;
};

/**
 * Adds new command to sortables
 *
 * @param {String} groupName The name of the API commands group
 * @param {String} commandName The name of the command
 * @param {Object} command The command itself
 */
VMS.Console.Site.addSortable = function(groupName, commandName, command) {

	var sortable = this.createSortable(
		groupName,
		commandName,
		command,
		{}
	);

	$('#sortable').append(sortable);
	this.setUpDhtmlPlugins();
	this.hideAndShow(sortable);
};


/**
 * Chnage the content of a sortable DOM element according to the response JSON.
 *
 * @param {Object} sortable the existing sortable element
 * @param {String} groupName API service commands group name
 * @param {String} commandName command name
 * @param {Object} requestJSON the commmand itself as it was sent.
 * @param {Object} responseJSON the reponse JSON from the service.
 */
VMS.Console.Site.changeSortable = function(sortable, groupName, commandName, requestJSON, responseJSON) {

	var newSortable = this.createSortable(
		groupName,
		commandName,
		requestJSON,
		responseJSON
	);

	$(sortable).replaceWith(newSortable);
	this.setUpDhtmlPlugins();
	this.hideAndShow(newSortable);
};



/**
 * Run one command from the sortables.
 *
 * @param {Object} sortable The command to run
 * @param {Object} nextCommands An array of next commands to run
 */
VMS.Console.Site.runOneCommand = function(sortable, nextCommands) {

	var groupName = sortable.CommandData.groupName;
	var command = sortable.CommandData.requestJSON;
	var serviceUrl = this.getServiceUrl(groupName);
	var returnStatusPropertyName = VMS.Test.ReturnStatusPropertyName[groupName];

	$('.td-dropdown', $(sortable)[0]).html('<img src="img/ajax-loader.gif" border="0" />');

	var me = this;

	var callback = function(textStatus, responseJSON) {

		if (! _.has(responseJSON, returnStatusPropertyName)) {

			responseJSON[returnStatusPropertyName] = -1;
			responseJSON["ErrorMessage"] = "Malformed JSON result.";
		}

		if (textStatus !== 'success') {

			responseJSON[returnStatusPropertyName] = -2;
			responseJSON["ErrorMessage"] = "Response status: " + textStatus;
		}


		me.changeSortable(
			sortable,
			groupName,
			command.ServiceName,
			command,
			responseJSON);

		if (nextCommands && nextCommands.length) {
			var nextCommand = nextCommands.shift();
			me.runOneCommand(nextCommand, nextCommands);
		} else {
			$('#btn-runall').removeAttr('disabled');
		}

	};


	try {

		if (command.ServiceName === "INT_SLEEP") {

			sortable.CommandData.responseJSON[returnStatusPropertyName] = 0;

			setInterval(
				function() {

					me.changeSortable(
						sortable,
						groupName,
						command.ServiceName,
						command,
						sortable.CommandData.responseJSON);

					if (nextCommands && nextCommands.length) {
						var nextCommand = nextCommands.shift();
						me.runOneCommand(nextCommand, nextCommands);
					} else {
						$('#btn-runall').removeAttr('disabled');
					}
				},
				command.SleepMS
			);

		} else {

			if (me.stopRunCommands) {
				// Flag to stop running commands was raised up.
				// Eventually means: the user deleted the commands on the UI.
				// Stop running the commands, there's no need to do the requests.
				//
				me.stopRunCommands = false;
				$('#btn-runall').removeAttr('disabled');

			} else {

				$.postJSON(serviceUrl, JSON.stringify(command), callback);
			}
		}


	} catch (e) {

		var responseJSON = sortable.CommandData.responseJSON;

		responseJSON[returnStatusPropertyName] = -3;
		responseJSON["ErrorMessage"] = e.message;

		me.changeSortable(
			sortable,
			groupName,
			command.ServiceName,
			command,
			responseJSON);

		if (nextCommands && nextCommands.length) {
			var nextCommand = nextCommands.shift();
			me.runOneCommand(nextCommand, nextCommands);
		} else {
			$('#btn-runall').removeAttr('disabled');
		}
	}

};





/**
 * Event handler when link 'Delete' was clicked.
 *
 * @param lnk
 */
VMS.Console.Site.onDeleteOneClicked = function(lnk) {

	var sortableItem = $(lnk).parents('li.sortable-item');

	this.hideAndShow(sortableItem);

	setTimeout(function() {
		sortableItem.remove();
	}, 200);

};


/**
 * Event handler when button 'Delete All' was clicked.
 *
 * @param lnk
 */
VMS.Console.Site.onDeleteAllClicked = function(lnk) {

	/**
	 * If running a set of tests, stop!
	 * See: runOneCommand
	 */
	if ($('#btn-runall').attr('disabled') === 'disabled') {
		this.stopRunCommands = true;
	}
	this.hideAndShow($('#sortable'));

	setTimeout(function() {
		$('#sortable').html('');
		//$('#btn-runall').removeAttr('disabled');
	}, 300);

};





/**
 * Event handler when button 'Run All' was clicked.
 *
 * @param btn
 */
VMS.Console.Site.onRunAllClicked = function(btn) {

	if ($('#btn-runall').attr('disabled') === 'disabled') {
		return;
	}

	var commands = [];
	$.each($('#sortable li.sortable-item'),
		function(index, command) {
			commands.push(command);
		});

	if (commands.length) {
		var nextCommand = commands.shift();
		$('#btn-runall').attr('disabled', 'disabled');
		this.stopRunCommands = false;
		this.runOneCommand(nextCommand, commands);
	}

};


/**
 * Event handler when link 'Run' was clicked.
 *
 * @param lnk
 */
VMS.Console.Site.onRunOneClicked = function(lnk) {

	if (this.stopRunCommands) { return; }

	// $('#btn-runall').attr('disabled', 'disabled');

	var sortable = $(lnk).parents('li.sortable-item')[0];

	this.runOneCommand(sortable);


};




//VMS.Console.Site.progressBar = function(countAllCommands) {
//
//	var currentCommandNumber = 0;
//
//
//	function show()
//	{
//		$('#div-progressbar').show();
//		$('#progressbar').progressbar( "value" , 0);
//	}
//
//	function hide()
//	{
//		$('#div-progressbar').hide();
//		$('#progressbar').progressbar( "value" , 0);
//
//	}
//
//	function progress()
//	{
//		currentCommandNumber++;
//
//		var value = (countAllCommands > 0)
//			? currentCommandNumber / countAllCommands
//			: 0;
//
//		value = Math.round(value * 100);
//		console.log("Progress: " + value + " %");
//
//		$('#div-progressbar').show();
//		$('#progressbar').progressbar( "value" , value);
//
//		if (value === 0) {
//			hide();
//		}
//	}
//
//	show();
//
//	return {
//		hide: hide,
//		progress: progress
//	}
//}

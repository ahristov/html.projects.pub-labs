/**
 * VMS Test. Contains API JSON commands used thru the tests.
 *
 * @class VMS.Test
 *
 * @author Atanas Hristov
 * @docauthor Atanas Hristov
 */

VMS.namespace('VMS.Test');

/**
 * Default values for JSON commands,
 * used to initialize the VMS.Test.Commands.
 *
 * @property {Object}
 */
VMS.Test.Defaults = {
	ScheduleInfo : {
		DvrID : 1,
		FiosID : 1,
		ParentID : 1,
		SeriesID : 1,
		SchedType : 1,
		ProgName : "",
		ChNum : 1,
		IsAdultTitle : false,
		BumpFlags : 1,
		RecDate : 1,
		RecDuration : 1
	},
	ScheduleDetails : {
		DvrID : 1,
		FiosID : 1,
		ParentID : 1,
		SeriesID : 1,
		SchedType : 1,
		ProgName : "",
		ChNum : 1,
		ChName : "",
		ProgType : 1,
		Rating : 1,
		IsAdultTitle : false,
		RecQuality : 1,
		BumpFlags : 1,
		Qualifiers : 1,
		RecDate : 1,
		RecDuration : 1,
		EarlyMin : 1,
		LateMin : 1,
		ProgDesc : ""
	},
	SolutionInfo : {
		DvrID : 1,
		FiosID : 1,
		SchedName : "",
		ChNum : 1,
		ChName : "",
		IsAdultTitle : false
	},
	ScheduleInfoWithRcdFlags : {
		DvrID : 1,
		ParentID : 1,
		ServiceID : 1,
		StartTime : 1,
		SchedType : 1,
		BumpFlags : 1,
		RecFlag : 1
	},
	SeriesInfo : {
		DvrID : 1,
		SeriesID : 1,
		ProgName : "",
		IsAdultTitle : false
	},
	SeriesDetails : {
		DvrID : 1,
		SeriesID : 1,
		ProgName : "",
		ChNum : 1,
		ChName : "",
		ProgType : 1,
		Rating : 1,
		IsAdultTitle : false,
		RecDate : 1,
		RecDuration : 1,
		Episodes : 1,
		RecQuality : 1,
		Keep : 1,
		Save : 1,
		Channel : 1,
		AirTime : 1,
		Duplicates : 1,
		StartTime : 1,
		EndTime : 1,
		DayTimes : 1
	},
	Settings : {
		SeriesOrder : 1,
		Episodes : 1,
		ReqQuality : 1,
		Keep : 1,
		Save : 1,
		Channel : 1,
		AirTime : 1,
		Duplicates : 1,
		StartTime : 1,
		EndTime : 1
	},
	RecordingInfo : {
		DvrID : 1,
		FiosID : 1,
		RecType : 1,
		ProgName : "",
		IsAdultTitle : false,
		ChNum : 1,
		ChName : "",
		Flags : 1,
		RecDate : 1,
		RecDuration : 1
	},
	RecordingEntry : {
		RecordingInfo: {}, //TODO
		FolderCount : 1,
		FolderListEntries : 1
		// ?? RecordList : RecordInfo JSON (Array)
	},
	RecordingDetails : {
		DvrID : 1,
		FiosID : 1,
		ParentID : 1,
		SeriesID : 1,
		RecType : 1,
		ProgName : "",
		IsAdultTitle : false,
		ChNum : 1,
		ChName : "",
		ProgType : 1,
		Rating : 1,
		Flags : 1,
		Bookmark : 1,
		PlayStatus : 1,
		RecQuality : 1,
		Qualifiers : 1,
		RecDate : 1,
		RecDuration : 1,
		ProgDate : 1,
		Duration : 1,
		EstimatedDelTime : 1,
		EarlyMinutes : 1,
		LateMinutes : 1,
		ProgDesc : "",
		EpisodeName : "",
		CastInfo : "",
		MovieYear : "",
		HasAdjShows : 1
	},
	RecordDetailsEntry :	{
		RecordingDetails: {}, //TODO
		FolderCount : 1,
		FolderListEntries : 1
		// ??? FolderList  RecordingDetails JSON Object (Array)
	},
	PlaybackProgDetails : {
		DvrID : 1,
		FiosID : 1,
		ParentID : 1,
		SeriesID : 1,
		RecType : 1,
		ProgName : "",
		IsAdultTitle : false,
		ChNum : 1,
		ChName : "",
		ProgType : 1,
		Rating : 1,
		Flags : 1,
		Qualifiers : 1,
		RecDate : 1,
		RecDuration : 1,
		ProgDate : 1,
		Duration : 1,
		ProgDesc : "",
		EpisodeName : "",
		CastInfo : "",
		MovieYear : "",
		HasAdjShows : 1
	},
	HistoryInfo : {
		DvrID : 1,
		FiosID : 1,
		ProgName : "",
		IsAdultTitle : false,
		ChNum : 1,
		ChName : "",
		Flags : 1,
		Cause : 1,
		Source : 1,
		EventDate : 1,
		RecDate : 1,
		RecDuration : 1,
		GuideTime : 1,
		GuideDuration : 1
	}
};



/**
 * JSON Commands to send to the VMS API alongs with default values.
 *
 * @property {Object}
 */

VMS.Test.Commands = {

	// Internal commands of the test suite
	MISC : {
		INT_SLEEP : {
			"ServiceName":"INT_SLEEP",
			"ServiceID":10000,
			"SleepMS":10000
		}
	},

	TESTVZPAL : {
		VZPAL_GetGPSTime : {
			"ServiceName":"VZPAL_GetGPSTime",
			"ServiceID":1000
		},
		VZPAL_GetTimeSpec : {
			"ServiceName":"VZPAL_GetTimeSpec",
			"ServiceID":1001
		},
		VZPAL_GetTimeStamp : {
			"ServiceName":"VZPAL_GetTimeStamp",
			"ServiceID":1002
		},
		VZPAL_ConvertGPSToTimespec : {
			"ServiceName":"VZPAL_ConvertGPSToTimespec",
			"ServiceID":1003
		},
		VZPAL_ConvertTimespecToGPS : {
			"ServiceName":"VZPAL_ConvertTimespecToGPS",
			"ServiceID":1004
		},
		VZPAL_GetLocalTimeFromGPS : {
			"ServiceName":"VZPAL_GetLocalTimeFromGPS",
			"ServiceID":1005
		},
		VZPAL_GetGPSFromLocalTime : {
			"ServiceName":"VZPAL_GetGPSFromLocalTime",
			"ServiceID":1006
		},
		VZPAL_GetSysTickTime : {
			"ServiceName":"VZPAL_GetSysTickTime",
			"ServiceID":1007
		},
		VZPAL_GetClockTicks : {
			"ServiceName":"VZPAL_GetClockTicks",
			"ServiceID":1008
		},
		VZPAL_GetTicksPerSecond : {
			"ServiceName":"VZPAL_GetTicksPerSecond",
			"ServiceID":1009
		},
		VZPAL_GetMillisecondsPerTick : {
			"ServiceName":"VZPAL_GetMillisecondsPerTick",
			"ServiceID":1010
		},
		VZPAL_GetMcardMacAddr : {
			"ServiceName":"VZPAL_GetMcardMacAddr",
			"ServiceID":1011
		},
		VZPAL_GetChannelMap : {
			"ServiceName":"VZPAL_GetChannelMap",
			"ServiceID":1012
		},
		VZPAL_TuneToChannel : {
			"ServiceName":"VZPAL_TuneToChannel",
			"ServiceID":1013,
			"TunerID":1,
			"ChannelNumber":4
		},
		VZPAL_DvrGetNumOfRecordings : {
			"ServiceName":"VZPAL_DvrGetNumOfRecordings",
			"ServiceID":1014
		},
		VZPAL_DvrGetRecordingsList : {
			"ServiceName":"VZPAL_DvrGetRecordingsList",
			"ServiceID":1015
		},
		VZPAL_StartRecording : {
			"ServiceName":"VZPAL_StartRecording",
			"ServiceID":1016,
			"TunerID":1
		},
		VZPAL_StopRecording : {
			"ServiceName":"VZPAL_StopRecording",
			"ServiceID":1017,
			"TunerID":1
		},
		VZPAL_GetBoxPlatformID : {
			"ServiceName":"VZPAL_GetBoxPlatformID",
			"ServiceID":1018
		},
		VZPAL_BoxHasDisk : {
			"ServiceName":"VZPAL_BoxHasDisk",
			"ServiceID":1019
		},
		VZPAL_GetHostSerialNo : {
			"ServiceName":"VZPAL_GetHostSerialNo",
			"ServiceID":1020
		},
		VZPAL_GetHostMacAddr : {
			"ServiceName":"VZPAL_GetHostMacAddr",
			"ServiceID":1021
		},
		VZPAL_MCardGetSerialNo : {
			"ServiceName":"VZPAL_MCardGetSerialNo",
			"ServiceID":1022
		},
		VZPAL_GetOSVerNo : {
			"ServiceName":"VZPAL_GetOSVerNo",
			"ServiceID":1023
		},
		VZPAL_IsBoxConnected : {
			"ServiceName":"VZPAL_IsBoxConnected",
			"ServiceID":1024
		},
		VZPAL_GetFirmwareVerNo : {
			"ServiceName":"VZPAL_GetFirmwareVerNo",
			"ServiceID":1025
		},
		VZPAL_TuneToBackground : {
			"ServiceName":"VZPAL_TuneToBackground",
			"ServiceID":1027,
			"TunerID":1,
			"ChannelNumber":4
		},
		VZPAL_QueryUserChannelStatus : {
			"ServiceName":"VZPAL_QueryUserChannelStatus",
			"ServiceID":1031,
			"TunerID":1
		},
		VZPAL_GetSourceForVCN : {
			"ServiceName":"VZPAL_GetSourceForVCN",
			"ServiceID":1032,
			"VCN":11
		},
		VZPAL_GetVCNForSourceID : {
			"ServiceName":"VZPAL_GetVCNForSourceID",
			"ServiceID":1033,
			"SourceID":129
		},
		VZPAL_GetUserChannelNumber : {
			"ServiceName":"VZPAL_GetUserChannelNumber",
			"ServiceID":1034,
			"TunerID":1
		},
		VZPAL_GetChannelType : {
			"ServiceName":"VZPAL_GetChannelType",
			"ServiceID":1035,
			"ChannelNumber":11
		},
		VZPAL_GetNumberOfQAMTuners : {
			"ServiceName":"VZPAL_GetNumberOfQAMTuners",
			"ServiceID":1036
		},
		VZPAL_Malloc : {
			"ServiceName":"VZPAL_Malloc",
			"ServiceID":1037
		},
		VZPAL_DvrStartPlayback : {
			"ServiceName":"VZPAL_DvrStartPlayback",
			"ServiceID":1039
		},
		VZPAL_DvrStopPlayback : {
			"ServiceName":"VZPAL_DvrStopPlayback",
			"ServiceID":1040
		},
		VZPAL_DvrAllocateLOD : {
			"ServiceName":"VZPAL_DvrAllocateLOD",
			"ServiceID":1041
		},
		VZPAL_DvrStartLOD : {
			"ServiceName":"VZPAL_DvrStartLOD",
			"ServiceID":1042,
			"TunerID":1
		},
		VZPAL_DvrStopLOD : {
			"ServiceName":"VZPAL_DvrStopLOD",
			"ServiceID":1043
		},
		VZPAL_DvrGetTotalRecSpace : {
			"ServiceName":"VZPAL_DvrGetTotalRecSpace",
			"ServiceID":1046
		},
		VZPAL_DvrGetFreeRecSpace : {
			"ServiceName":"VZPAL_DvrGetFreeRecSpace",
			"ServiceID":1047
		},
		VZPAL_DvrGetRecDuration : {
			"ServiceName":"VZPAL_DvrGetRecDuration",
			"ServiceID":1048
		},
		VZPAL_DvrDeleteRecProgram : {
			"ServiceName":"VZPAL_DvrDeleteRecProgram",
			"ServiceID":1049
		}
	},

	VOD : {
		PlayAsset : {
			"ServiceName":"PlayAsset",
			"ClientID":"123",
			"MACID":"123",
			"AssetID":123,
			"PID":"123",
			"PAID":"123",
			"ServiceType":"123",
			"ResumePoint":123,
			"AdsEnable":123,
			"PurchaseMode":123
		},
		PauseAsset : {
			"ServiceName":"PauseAsset",
			"ClientID":"123",
			"MACID":"123"
		},
		Resume : {
			"ServiceName":"Resume",
			"ClientID":"123",
			"MACID":"123"
		},
		FFWDAsset : {
			"ServiceName":"FFWDAsset",
			"ClientID":"123",
			"MACID":"123",
			"ScaleOverride":123
		},
		RWNDAsset : {
			"ServiceName":"RWNDAsset",
			"ClientID":"123",
			"MACID":"123",
			"ScaleOverride":123
		},
		StopAsset : {
			"ServiceName":"StopAsset",
			"ClientID":"123",
			"MACID":"123"
		},
		JumpToNPTPosition : {
			"ServiceName":"JumpToNPTPosition",
			"ClientID":"123",
			"MACID":"123",
			"JumpNPTPos":123
		},
		GetNPT : {
			"ServiceName":"GetNPT",
			"ClientID":"123",
			"MACID":"123"
		},
		SetFFWDScale : {
			"ServiceName":"SetFFWDScale",
			"ClientID":"123",
			"MACID":"123",
			"FFWDScale":123
		},
		SetRWNDScale : {
			"ServiceName":"SetRWNDScale",
			"ClientID":"123",
			"MACID":"123",
			"RWNDScale":123
		},
		SetFFWDSpeed : {
			"ServiceName":"SetFFWDSpeed",
			"ClientID":"123",
			"MACID":"123",
			"FFWDSpeed":123
		},
		SetRWNDSpeed : {
			"ServiceName":"SetRWNDSpeed",
			"ClientID":"123",
			"MACID":"123",
			"RWNDSpeed":123
		},
		CheckERS : {
			"ServiceName":"CheckERS",
			"ClientID":"123",
			"MACID":"123"
		}
	},

	DVR : {
		GetScheduleList : {
			"ServiceName":"GetScheduleList",
			"ClientID":"1",
			"StartIndex":123,
			"NumOfEntries":123
		},
		GetScheduleDetails : {
			"ServiceName":"GetScheduleDetails",
			"ClientID":"1",
			"ServiceID":1001,
			"StartTime":123
		},
		GetScheduleListWDetails : {
			"ServiceName":"GetScheduleListWDetails",
			"ClientID":"1",
			"StartIndex":123,
			"NumOfEntries":123
		},
		GetScheduleListWRecflag : {
			"ServiceName":"GetScheduleListWRecflag",
			"ClientID":"1",
			"StartTime":123,
			"EndTime":123
		},
		CreateScheduleFromEPG : {
			"ServiceName":"CreateScheduleFromEPG",
			"ClientID":"1",
			"ServiceID":1001,
			"StartTime":123,
			"Duration":123
		},
		ModifySchedule : {
			"ServiceName":"ModifySchedule",
			"ClientID":"1",
			"DvrID":123,
			"EarlyMinutes":123,
			"LateMinutes":123
		},
		CancelSchedule : {
			"ServiceName":"CancelSchedule",
			"ClientID":"1",
			"DvrID":123
		},
		GetSeriesList : {
			"ServiceName":"GetSeriesList",
			"ClientID":"1"
		},
		GetSeriesDetails : {
			"ServiceName":"GetSeriesDetails",
			"ClientID":"1",
			"DvrID":123
		},
		GetSeriesListWDetails : {
			"ServiceName":"GetSeriesListWDetails",
			"ClientID":"1",
			"StartIndex":123,
			"NumOfEntries":123
		},
		CreateSeries : {
			"ServiceName":"CreateSeries",
			"ClientID":"1",
			"ServiceID":1001,
			"SeriesID":123,
			"StartTime":123,
			"Duration":123,
			"SeriesName":"123"
		},
		CreateSeriesWOptions : {
			"ServiceName":"CreateSeriesWOptions",
			"ClientID":"1",
			"ServiceID":1001,
			"SeriesID":123,
			"StartTime":123,
			"Duration":123,
			"SeriesName":"123",
			"Settings":{} //TODO:Settings
		},
		ModifySeries : {
			"ServiceName":"ModifySeries",
			"ClientID":"1",
			"DvrID":123,
			"Settings":{} //TODO:Settings
		},
		ModifySeriesPriority : {
			"ServiceName":"ModifySeriesPriority",
			"ClientID":"1",
			"DvrID":123,
			"NewPriority":123
		},
		CancelSeries : {
			"ServiceName":"CancelSeries",
			"ClientID":"1",
			"DvrID":123
		},
		GetSeriesDefSettings : {
			"ServiceName":"GetSeriesDefSettings",
			"ClientID":"1"
		},
		ModifySeriesDefSettings : {
			"ServiceName":"ModifySeriesDefSettings",
			"ClientID":"1",
			"Settings":{} //TODO:Settings
		},
		GetSeriesSettings : {
			"ServiceName":"GetSeriesSettings",
			"ClientID":"1",
			"DvrID":123
		},
		CreateManualSchedule : {
			"ServiceName":"CreateManualSchedule",
			"ClientID":"1",
			"ServiceID":1001,
			"StartTime":123,
			"Duration":123
		},
		CreateManualSeries : {
			"ServiceName":"CreateManualSeries",
			"ClientID":"1",
			"ServiceID":1001,
			"StartTime":123,
			"Duration":123,
			"Keep":123,
			"Save":123,
			"Recurring":123
		},
		ModifyManualSeries : {
			"ServiceName":"ModifyManualSeries",
			"ClientID":"1",
			"DvrID":123,
			"StartTime":123,
			"Duration":123,
			"Keep":123,
			"Save":123,
			"Recurring":123
		},
		GetRecordedList : {
			"ServiceName":"GetRecordedList",
			"ClientID":"1",
			"NumOfEntries":123,
			"GroupByFlag":123,
			"MatchStr":"123"
		},
		GetRecordedPgmDetails : {
			"ServiceName":"GetRecordedPgmDetails",
			"ClientID":"1",
			"DvrID":123
		},
		GetRecordedListWDetails : {
			"ServiceName":"GetRecordedListWDetails",
			"ClientID":"1",
			"StartIndex":123,
			"NumberOfEntries":123,
			"MatchStr":"123"
		},
		ModifyRecPgmFlag : {
			"ServiceName":"ModifyRecPgmFlag",
			"ClientID":"1",
			"DvrID":123,
			"DeleteFlag":123
		},
		DeleteRecPgm : {
			"ServiceName":"DeleteRecPgm",
			"ClientID":"1",
			"DeleteByFlag":123,
			"DvrID":123,
			"MatchStr":"123"
		},
		GetActiveRecording : {
			"ServiceName":"GetActiveRecording",
			"ClientID":"1"
		},
		ExtendActiveRecording : {
			"ServiceName":"ExtendActiveRecording",
			"ClientID":"1",
			"DvrID":123,
			"ExtendTime":123
		},
		StopRecording : {
			"ServiceName":"StopRecording",
			"ClientID":"1",
			"DvrID":123
		},
		OpenPlayback : {
			"ServiceName":"OpenPlayback",
			"ClientID":"1",
			"DvrID":123,
			"StartPos":123
		},
		ClosePlayback : {
			"ServiceName":"ClosePlayback",
			"ClientID":"1",
			"DvrID":123,
			"StopPos":123
		},
		UpdatePlaybackPos : {
			"ServiceName":"UpdatePlaybackPos",
			"ClientID":"1",
			"DvrID":123,
			"PlaybackPos":123
		},
		GetPlayProgInfo : {
			"ServiceName":"GetPlayProgInfo",
			"ClientID":"1",
			"DvrID":123
		},
		GetAdjPlaybackProgInfo : {
			"ServiceName":"GetAdjPlaybackProgInfo",
			"ClientID":"1",
			"DvrID":123
		},
		GetRecDeletedList : {
			"ServiceName":"GetRecDeletedList",
			"ClientID":"1",
			"StartIndex":123,
			"NumOfEntries":123
		},
		GetRctDeletedDetails : {
			"ServiceName":"GetRctDeletedDetails",
			"ClientID":"1",
			"DvrID":123
		},
		RestoreRctDeleted : {
			"ServiceName":"RestoreRctDeleted",
			"ClientID":"1",
			"RestoreByFlag":123,
			"DvrID":123,
			"MatchStr":"123"
		},
		DeleteRctDeleted : {
			"ServiceName":"DeleteRctDeleted",
			"ClientID":"1",
			"DeleteByFlag":123,
			"DvrID":123,
			"MatchStr":"123"
		},
		GetHistoryList : {
			"ServiceName":"GetHistoryList",
			"ClientID":"1",
			"StartIndex":123,
			"NumOfEntries":123
		},
		GetHistoryDetails : {
			"ServiceName":"GetHistoryDetails",
			"ClientID":"1",
			"DvrID":123
		},
		DeleteHistory : {
			"ServiceName":"DeleteHistory",
			"ClientID":"1",
			"DeleteByFlag":123,
			"DvrID":123,
			"MatStr":"123"
		},
		GetDeviceRecStatus : {
			"ServiceName":"GetDeviceRecStatus",
			"ClientID":"1",
			"DeviceID":123
		}
	},

	EPG : {
		GetListOfAvailableData : {
			"ServiceName":"GetListOfAvailableData",
			"ClientID":"1"
		},
		DownloadScheduleData : {
			"ServiceName":"DownloadScheduleData",
			"ClientID":"1",
			"StartTime":1,
			"Duration":1,
			"StartChannelNumber":4,
			"NumberOfChannels":123
		},
		DownloadChannelData : {
			"ServiceName":"DownloadChannelData",
			"ClientID":"1"
		},
		DownloadLKPData : {
			"ServiceName":"DownloadLKPData",
			"ClientID":"1"
		}
	},

	TUNER : {
		GetChannelMap : {
			"ServiceName":"GetChannelMap",
			"ClientID":"1"
		},
		Tune : {
			"ServiceName":"Tune",
			"ClientID":"1",
			"ChannelNumber":4
		}
	},

	UTILITY : {
		GetChannelSubscribedPackageList : {
			"ServiceName":"GetChannelSubscribedPackageList",
			"ClientID":"1"
		},
		IsPackageIDSubscribed : {
			"ServiceName":"IsPackageIDSubscribed",
			"ClientID":"1",
			"PackageID":4
		},
		GetAllSubscribedPackageList : {
			"ServiceName":"GetAllSubscribedPackageList",
			"ClientID":"1"
		}
	}
};





/**
 * Which are the inputs the user has to manually enter data
 * in the test console when sending manually a command to the VMS.
 *
 * Note: The rest of the command parameters will be taken automatically
 * from VMS.Test.Commands. These are only the names of those parameters
 * where a manual input from the user is expected.
 *
 * @property {Object}
 */

VMS.Test.Inputs = {
	TESTVZPAL : {
		VZPAL_TuneToChannel 			: ["TunerID","ChannelNumber"],
		VZPAL_StartRecording 			: ["TunerID"],
		VZPAL_StopRecording				: ["TunerID"],
		VZPAL_TuneToBackground			: ["TunerID","ChannelNumber"],
		VZPAL_QueryUserChannelStatus 	: ["TunerID"],
		VZPAL_GetSourceForVCN 			: ["VCN"],
		VZPAL_GetVCNForSourceID 		: ["SourceID"],
		VZPAL_GetUserChannelNumber 		: ["TunerID"],
		VZPAL_GetChannelType 			: ["ChannelNumber"],
		// VZPAL_DvrStartPlayback 			: ["ConID","ClientID"],
		VZPAL_DvrStartLOD 				: ["TunerID"]
		// VZPAL_DvrStopLOD 				: ["TunerID"],
		// VZPAL_DvrDeleteRecProgram 		: ["ConID"]
	},

	VOD : {
		PlayAsset 						: ["MACID","AssetID","PID","PAID",
											"ServiceType","ResumePoint","AdsEnable","PurchaseMode" ],
		PauseAsset 						: ["MACID"],
		Resume 							: ["MACID"],
		FFWDAsset 						: ["MACID","ScaleOverride"],
		RWNDAsset 						: ["MACID","ScaleOverride"],
		StopAsset 						: ["MACID"],
		JumpToNPTPosition 				: ["MACID","JumpNPTPos"],
		GetNPT 							: ["MACID"],
		SetFFWDScale 					: ["MACID","FFWDScale"],
		SetRWNDScale 					: ["MACID","RWNDScale"],
		SetFFWDSpeed 					: ["MACID","FFWDSpeed"],
		SetRWNDSpeed 					: ["MACID","RWNDSpeed"],
		CheckERS 						: ["MACID"]
	},

	DVR : {
		GetScheduleList 				: ["StartIndex","NumOfEntries"],
		GetScheduleDetails 				: ["StartTime"],
		GetScheduleListWDetails 		: ["StartIndex","NumOfEntries"],
		GetScheduleListWRecflag 		: ["StartTime","EndTime"],
		CreateScheduleFromEPG 			: ["StartTime","Duration"],
		ModifySchedule 					: ["DvrID","EarlyMinutes","LateMinutes"],
		CancelSchedule 					: ["DvrID"],
		GetSeriesDetails 				: ["DvrID"],
		GetSeriesListWDetails 			: ["StartIndex","NumOfEntries"],
		CreateSeries 					: ["SeriesID","StartTime","Duration","SeriesName"],
		CreateSeriesWOptions 			: ["SeriesID","StartTime","Duration","SeriesName"],
										// "Settings":{} //TODO:Settings
		ModifySeries 					: ["DvrID"],
										// "Settings":{} //TODO:Settings
		ModifySeriesPriority 			: ["DvrID","NewPriority"],
		CancelSeries 					: ["DvrID"],
		// ModifySeriesDefSettings 		: [],
										// "Settings":{} //TODO:Settings
		GetSeriesSettings 				: ["DvrID"],
		CreateManualSchedule 			: ["StartTime","Duration"],
		CreateManualSeries 				: ["StartTime","Duration","Keep","Save","Recurring"],
		ModifyManualSeries 				: ["DvrID","StartTime","Duration","Keep","Save","Recurring"],
		GetRecordedList 				: ["NumOfEntries","GroupByFlag","MatchStr"],
		GetRecordedPgmDetails 			: ["DvrID"],
		GetRecordedListWDetails 		: ["StartIndex","NumberOfEntries","MatchStr"],
		ModifyRecPgmFlag 				: ["DvrID","DeleteFlag"],
		DeleteRecPgm 					: ["DeleteByFlag","DvrID","MatchStr"],
		ExtendActiveRecording 			: ["DvrID","ExtendTime"],
		StopRecording 					: ["DvrID"],
		OpenPlayback 					: ["DvrID","StartPos"],
		ClosePlayback 					: ["DvrID","StopPos"],
		UpdatePlaybackPos 				: ["DvrID","PlaybackPos"],
		GetPlayProgInfo 				: ["DvrID"],
		GetAdjPlaybackProgInfo 			: ["DvrID"],
		GetRecDeletedList 				: ["StartIndex","NumOfEntries"],
		GetRctDeletedDetails 			: ["DvrID"],
		RestoreRctDeleted 				: ["RestoreByFlag","DvrID","MatchStr"],
		DeleteRctDeleted 				: ["DeleteByFlag","DvrID","MatchStr"],
		GetHistoryList 					: ["StartIndex","NumOfEntries"],
		GetHistoryDetails 				: ["DvrID"],
		DeleteHistory 					: ["DeleteByFlag","DvrID","MatStr"],
		GetDeviceRecStatus 				: ["DeviceID"]
	},

	EPG : {
		DownloadScheduleData 			: ["StartTime","Duration","StartChannelNumber","NumberOfChannels"]
	},

	TUNER : {
		Tune 							: ["ChannelNumber"]
	},

	UTILITY : {
		IsPackageIDSubscribed 			: ["PackageID"]
	}
};



/**
 * Which is the name of the property of the returned
 * from the VMS API JSON which to be evaluated for assertions (0 means OK, else means ERROR).
 *
 * NOTE: some commands return `ReturnValue`, some `StatusCode`.
 *
 * @property {Object}
 */

VMS.Test.ReturnStatusPropertyName = {
	TESTVZPAL : "ReturnValue",
	VOD : "StatusCode",
	DVR : "StatusCode",
	EPG : "StatusCode",
	TUNER : "StatusCode",
	UTILITY : "StatusCode"
};



/**
 * Scripts to run thru the UI Test Console.
 *
 * For each end every API service group it defines an array of commands
 * to run in the exact order on the Console.
 *
 * @property {Object}
 */

VMS.Test.ConsoleScripts = {
	TESTVZPAL : [
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetGPSTime                   	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetTimeSpec                  	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetTimeStamp                 	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_ConvertGPSToTimespec         	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_ConvertTimespecToGPS         	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetLocalTimeFromGPS          	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetGPSFromLocalTime          	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetSysTickTime               	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetClockTicks                	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetTicksPerSecond            	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetMillisecondsPerTick       	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetMcardMacAddr              	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetChannelMap                	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_TuneToChannel                	,
		VMS.Test.Commands.MISC.INT_SLEEP								,
		VMS.Test.Commands.TESTVZPAL.VZPAL_DvrGetNumOfRecordings        	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_DvrGetRecordingsList         	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetBoxPlatformID             	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_BoxHasDisk                   	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetHostSerialNo              	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetHostMacAddr               	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_MCardGetSerialNo             	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetOSVerNo                   	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_IsBoxConnected               	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetFirmwareVerNo             	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_TuneToBackground				,
		VMS.Test.Commands.TESTVZPAL.VZPAL_QueryUserChannelStatus       	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetSourceForVCN              	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetVCNForSourceID            	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetUserChannelNumber         	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetChannelType               	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_GetNumberOfQAMTuners         	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_Malloc                       	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_DvrGetTotalRecSpace          	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_DvrGetFreeRecSpace           	,
		VMS.Test.Commands.TESTVZPAL.VZPAL_DvrGetRecDuration
	],

	VOD : [
		VMS.Test.Commands.VOD.PauseAsset								,
		VMS.Test.Commands.VOD.Resume                                   	,
		VMS.Test.Commands.VOD.FFWDAsset                                	,
		VMS.Test.Commands.VOD.RWNDAsset                                	,
		VMS.Test.Commands.VOD.StopAsset                                	,
		VMS.Test.Commands.VOD.SetFFWDScale                             	,
		VMS.Test.Commands.VOD.SetRWNDScale
	],

	DVR : [
		VMS.Test.Commands.DVR.GetScheduleListWRecflag					,
		VMS.Test.Commands.DVR.CreateScheduleFromEPG                    	,
		VMS.Test.Commands.DVR.CancelSeries                             	,
		VMS.Test.Commands.DVR.GetSeriesDefSettings                     	,
		VMS.Test.Commands.DVR.GetActiveRecording                       	,
		VMS.Test.Commands.DVR.GetAdjPlaybackProgInfo                   	,
		VMS.Test.Commands.DVR.RestoreRctDeleted                        	,
		VMS.Test.Commands.DVR.DeleteRctDeleted                         	,
		VMS.Test.Commands.DVR.GetHistoryList                           	,
		VMS.Test.Commands.DVR.GetDeviceRecStatus
	],

	EPG : [
		VMS.Test.Commands.EPG.GetListOfAvailableData					,
		VMS.Test.Commands.EPG.DownloadScheduleData                     	,
		VMS.Test.Commands.EPG.DownloadChannelData                      	,
		VMS.Test.Commands.EPG.DownloadLKPData
	],

	TUNER : [
		VMS.Test.Commands.TUNER.GetChannelMap
	],

	UTILITY : [
		VMS.Test.Commands.UTILITY.GetChannelSubscribedPackageList		,
		VMS.Test.Commands.UTILITY.IsPackageIDSubscribed
	]

};



/**
 * Manual Commands to run thru the UI Test Console
 *
 * @property {Object}
 */

VMS.Test.ConsoleManuals = {};
_.each(_.keys(VMS.Test.ConsoleScripts), function(groupName) {

	// create group
	VMS.Test.ConsoleManuals[groupName] = [];

	// add those already defined as ConsoleScripts
	_.each(VMS.Test.ConsoleScripts[groupName], function(command) {
		if (command.ServiceName !== "INT_SLEEP") {
			VMS.Test.ConsoleManuals[groupName].push(_.clone(command));
		}
	});

});

// Add more that we should have as manual commands for TESTVZPAL
(function() {
	var groupName = "TESTVZPAL";
	_.each([
		"VZPAL_StartRecording",
		"VZPAL_StopRecording",
		"VZPAL_DvrStartPlayback",
		"VZPAL_DvrStopPlayback",
		"VZPAL_DvrAllocateLOD",
		"VZPAL_DvrStartLOD",
		"VZPAL_DvrStopLOD",
		"VZPAL_DvrDeleteRecProgram"
	], function(commandName) {

		if (! _.find(VMS.Test.ConsoleManuals[groupName], function(command){ return command.ServiceName === commandName; })) {
			VMS.Test.ConsoleManuals[groupName].push(_.clone(VMS.Test.Commands[groupName][commandName]));
		}

	});
}());




// Add more that we should have as manual commands for VOD
(function() {
	var groupName = "VOD";
	_.each([
		"PlayAsset",
		"JumpToNPTPosition",
		"GetNPT",
		"SetFFWDSpeed",
		"SetRWNDSpeed",
		"CheckERS"
	], function(commandName) {

		if (! _.find(VMS.Test.ConsoleManuals[groupName], function(command){ return command.ServiceName === commandName; })) {
			VMS.Test.ConsoleManuals[groupName].push(_.clone(VMS.Test.Commands[groupName][commandName]));
		}

	});
}());

// Add more that we should have as manual commands for DVR
(function() {
	var groupName = "DVR";
	_.each([
		"GetScheduleList",
		"GetScheduleDetails",
		"GetScheduleListWDetails",
		"ModifySchedule",
		"CancelSchedule",
		"GetSeriesList",
		"GetSeriesDetails",
		"GetSeriesListWDetails",
		"CreateSeries",
		"CreateSeriesWOptions",
		"ModifySeries",
		"ModifySeriesPriority",
		"ModifySeriesDefSettings",
		"GetSeriesSettings",
		"CreateManualSchedule",
		"CreateManualSeries",
		"ModifyManualSeries",
		"GetRecordedList",
		"GetRecordedPgmDetails",
		"GetRecordedListWDetails",
		"ModifyRecPgmFlag",
		"DeleteRecPgm",
		"ExtendActiveRecording",
		"StopRecording",
		"OpenPlayback",
		"ClosePlayback",
		"UpdatePlaybackPos",
		"GetPlayProgInfo",
		"GetRecDeletedList",
		"GetRctDeletedDetails",
		"GetHistoryDetails",
		"DeleteHistory"
	], function(commandName) {

		if (! _.find(VMS.Test.ConsoleManuals[groupName], function(command){ return command.ServiceName === commandName; })) {
			VMS.Test.ConsoleManuals[groupName].push(_.clone(VMS.Test.Commands[groupName][commandName]));
		}

	});
}());

// Add more that we should have as manual commands for EPG
(function() {
	var groupName = "EPG";
	_.each([
	], function(commandName) {

		if (! _.find(VMS.Test.ConsoleManuals[groupName], function(command){ return command.ServiceName === commandName; })) {
			VMS.Test.ConsoleManuals[groupName].push(_.clone(VMS.Test.Commands[groupName][commandName]));
		}

	});
}());


// Add more that we should have as manual commands for TUNER
(function() {
	var groupName = "TUNER";
	_.each([
		"Tune"
	], function(commandName) {

		if (! _.find(VMS.Test.ConsoleManuals[groupName], function(command){ return command.ServiceName === commandName; })) {
			VMS.Test.ConsoleManuals[groupName].push(_.clone(VMS.Test.Commands[groupName][commandName]));
		}

	});
}());

// Add more that we should have as manual commands for TUNER
(function() {
	var groupName = "UTILITY";
	_.each([
		"GetAllSubscribedPackageList"
	], function(commandName) {

		if (! _.find(VMS.Test.ConsoleManuals[groupName], function(command){ return command.ServiceName === commandName; })) {
			VMS.Test.ConsoleManuals[groupName].push(_.clone(VMS.Test.Commands[groupName][commandName]));
		}

	});
}());


// Initialization of default values.
// Set values in VMS.Test.Commands from VMS.Test.Defaults
//
(function(){

	var groupNames = _.keys(VMS.Test.Commands);
	var defaults = VMS.Test.Defaults;

	_.each(groupNames, function(groupName) {

		var group = VMS.Test.Commands[groupName];
		var commandNames = _.keys(group);

		_.each(commandNames, function(commandName) {

			var cmd = group[commandName];

			_.each(_.keys(cmd), function(prop) {

				if (typeof cmd[prop] === 'object') {
					// console.log(prop.toString() + " " + typeof cmd[prop]);
					if (defaults[prop]) {
						// console.log('take defaults...');
						_.defaults(cmd[prop], defaults[prop])
					}
				}

			});
		});
	});

}());

